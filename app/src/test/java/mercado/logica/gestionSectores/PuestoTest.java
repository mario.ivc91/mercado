package mercado.logica.gestionSectores;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import mercado.logica.gestionLecturas.Medidor;

public class PuestoTest {
    @Test public void TestEquals() {
        Medidor medidor = new Medidor(null);
        Puesto puesto1 = new Puesto(1, 10, false, false, false, medidor);
        Puesto puesto2 = new Puesto(1, 10, false, false, false, medidor);
        
        assertEquals(true,puesto1.equals(puesto2));
    }

    @Test public void TestEquals2() {
        Medidor medidor = new Medidor(null);
        Puesto puesto1 = new Puesto(1, 10, false, false, false, medidor);
        Puesto puesto2 = new Puesto(2, 10, false, false, false, medidor);

        assertEquals(false,puesto1.equals(puesto2));
    }
}