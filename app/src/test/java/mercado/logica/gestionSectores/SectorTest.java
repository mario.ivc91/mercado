package mercado.logica.gestionSectores;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.logica.gestionSectores.Excepciones.PuestoExistenteException;
import mercado.logica.gestionSectores.Excepciones.PuestoInexistenteException;

public class SectorTest {
    @Test public void TestEquals() {
        Sector sector1 = new Sector(1,"ARTESANIAS", null);
        Sector sector2 = new Sector(1,"ARTESANIAS", null);
        
        assertEquals(true,sector1.equals(sector2));
    }

    @Test public void TestEquals2() {
        Sector sector1 = new Sector(1,"ARTESANIAS", null);
        Sector sector2 = new Sector(2,"COMIDAS", null);

        assertEquals(false,sector1.equals(sector2));
    }

    @Test public void agregarPuesto() throws PuestoExistenteException {
        Sector sector = new Sector(1,"ARTESANIAS", null);
        Puesto puesto = new Puesto(1, 10, false, false, false, null);
        sector.agregarPuesto(puesto);

        assertEquals(puesto,sector.getPuestos().get(0));
    }

    @Test public void eliminarPuesto() throws PuestoInexistenteException, PuestoExistenteException {
        Sector sector = new Sector(1,"ARTESANIAS", null);
        Puesto puesto = new Puesto(1, 10, false, false, false, null);
        sector.agregarPuesto(puesto);

        sector.eliminarPuesto(puesto);

        assertEquals(sector.getPuestos().size(),0,0);
    }

    @Test public void modificarPuesto() throws PuestoInexistenteException, PuestoExistenteException {
        Sector sector = new Sector(1,"ARTESANIAS", null);
        Puesto puesto1 = new Puesto(1, 10, false, false, false, null);
        sector.agregarPuesto(puesto1);
        Puesto puesto2 = new Puesto(1, 100, true, false, false, null);
        sector.modificarPuesto(puesto2);
        
        assertEquals(sector.getPuestos().get(0),puesto2);
    }
}