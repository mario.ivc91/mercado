package mercado.logica.gestionSectores;

import static org.junit.Assert.assertThrows;

import org.junit.Test;

import mercado.logica.gestionSectores.Excepciones.SectorExistenteException;
import mercado.logica.gestionSectores.Excepciones.SectorInexistenteException;

public class ExcepcionesColeccionSectoresTest {
    @Test public void agregarSector() throws SectorExistenteException {
        ColeccionSectores sectores = new ColeccionSectores();
        Sector sector = new Sector(1,"ARTESANIAS", null);
        sectores.agregarSector(sector);
        Sector sector2 = new Sector(1,"ARTESANIAS", null);
        
        assertThrows(SectorExistenteException.class,()->sectores.agregarSector(sector2));
    }

    @Test public void eliminarSector() throws SectorInexistenteException {
        ColeccionSectores sectores = new ColeccionSectores();
        Sector sector = new Sector(1,"ARTESANIAS", null);
        
        assertThrows(SectorInexistenteException.class,()->sectores.eliminarSector(sector));
    }
}