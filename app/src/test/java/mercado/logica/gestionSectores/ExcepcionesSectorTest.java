package mercado.logica.gestionSectores;

import static org.junit.Assert.assertThrows;

import org.junit.Test;

import mercado.logica.gestionSectores.Excepciones.PuestoExistenteException;
import mercado.logica.gestionSectores.Excepciones.PuestoInexistenteException;

public class ExcepcionesSectorTest {
    @Test public void agregarPuesto() throws PuestoExistenteException {
        Sector sector = new Sector(1,"ARTESANIAS", null);
        Puesto puesto1 = new Puesto(1, 10, false, false, false, null);
        sector.agregarPuesto(puesto1);
        Puesto puesto2 = new Puesto(1, 10, false, false, false, null);
        
        assertThrows(PuestoExistenteException.class,()->sector.agregarPuesto(puesto2));
    }

    @Test public void eliminarPuesto() throws PuestoInexistenteException {
        Sector sector = new Sector(1,"ARTESANIAS", null);
        Puesto puesto1 = new Puesto(1, 10, false, false, false, null);
        
        assertThrows(PuestoInexistenteException.class,()->sector.eliminarPuesto(puesto1));
    }
}