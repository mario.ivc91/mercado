package mercado.logica.gestionSectores;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.logica.gestionSectores.Excepciones.SectorExistenteException;
import mercado.logica.gestionSectores.Excepciones.SectorInexistenteException;

public class ColeccionSectoresTest {
    @Test public void agregarSector() throws SectorExistenteException {
        ColeccionSectores sectores = new ColeccionSectores();
        Sector sector = new Sector(1,"ARTESANIAS", null);
        sectores.agregarSector(sector);

        assertEquals(sector,sectores.getSectores().get(0));
    }

    @Test public void eliminarSector() throws SectorExistenteException, SectorInexistenteException {
        ColeccionSectores sectores = new ColeccionSectores();
        Sector sector = new Sector(1,"ARTESANIAS", null);
        sectores.agregarSector(sector);

        sectores.eliminarSector(sector);

        assertEquals(sectores.getSectores().size(),0,0);
    }

    @Test public void modificarSector() throws SectorExistenteException, SectorInexistenteException{
        ColeccionSectores sectores = new ColeccionSectores();
        Sector sector1 = new Sector(1,"ARTESANIAS", null);
        sectores.agregarSector(sector1);
        Sector sector2 = new Sector(1,"COMIDAS", null);
        sectores.modificarSector(sector2);
        
        assertEquals(sectores.getSectores().get(0),sector2);
    }
}