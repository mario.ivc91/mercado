package mercado.logica.gestionContratos;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import mercado.logica.gestionSectores.Puesto;
import mercado.logica.gestionContratos.Excepciones.ContratoExistenteException;
import mercado.logica.gestionContratos.Excepciones.ContratoInexistenteException;

public class FicheroContratosTest {
    @Test public void agregarContrato() throws ContratoExistenteException {
        FicheroContratos coleccionContratos = new FicheroContratos();
        Puesto puesto = new Puesto(1, 10, false, false, false, null);

        Contrato contrato = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 0, null, puesto, null, null);
        coleccionContratos.agregarContrato(contrato);

        assertEquals(coleccionContratos.getContratos().get(0),contrato);
    }

    @Test public void eliminarContrato() throws ContratoExistenteException, ContratoInexistenteException {
        FicheroContratos coleccionContratos = new FicheroContratos();
        Puesto puesto = new Puesto(1, 10, false, false, false, null);

        Contrato contrato = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 0, null, puesto, null, null);
        coleccionContratos.agregarContrato(contrato);

        assertEquals(coleccionContratos.getContratos().get(0),contrato);

        coleccionContratos.eliminarContrato(contrato);

        assertEquals(coleccionContratos.getContratos().size(), 0,0);
    }

    @Test public void modificarContrato() throws ContratoInexistenteException, ContratoExistenteException {
        FicheroContratos coleccionContratos = new FicheroContratos();
        Puesto puesto = new Puesto(1, 10, false, false, false, null);

        Contrato contrato = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 1000, null, puesto, null, null);
        coleccionContratos.agregarContrato(contrato);

        Contrato contrato2 = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 10000, null, puesto, null, null);
        coleccionContratos.modificarContrato(contrato2);

        assertEquals(contrato2,coleccionContratos.getContratos().get(0));
    }
}