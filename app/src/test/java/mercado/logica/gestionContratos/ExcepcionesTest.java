package mercado.logica.gestionContratos;

import static org.junit.Assert.assertThrows;

import java.time.LocalDate;

import org.junit.Test;

import mercado.logica.gestionSectores.Puesto;
import mercado.logica.gestionContratos.Excepciones.ContratoExistenteException;
import mercado.logica.gestionContratos.Excepciones.ContratoInexistenteException;

public class ExcepcionesTest {
    @Test public void agregarContrato() throws ContratoExistenteException {
        FicheroContratos coleccionContratos = new FicheroContratos();
        Puesto puesto = new Puesto(1, 10, false, false, false, null);

        Contrato contrato = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 0, null, puesto, null, null);
        coleccionContratos.agregarContrato(contrato);
        Contrato contrato2 = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 0, null, puesto, null, null);

        assertThrows(ContratoExistenteException.class,()->coleccionContratos.agregarContrato(contrato2));
    }

    @Test public void eliminarContrato() throws ContratoInexistenteException {
        FicheroContratos coleccionContratos = new FicheroContratos();
        Puesto puesto = new Puesto(1, 10, false, false, false, null);

        Contrato contrato = new Contrato(null, null, LocalDate.of(2024, 1, 1), null, 0, null, puesto, null, null);

        assertThrows(ContratoInexistenteException.class,()->coleccionContratos.eliminarContrato(contrato));
    }
}