package mercado.logica.gestionContratos;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import mercado.logica.gestionContratos.Contrato;
import mercado.logica.gestionSectores.Puesto;

public class ContratoTest {
    @Test public void TestCalcularPeriodoVigencia() {
        Contrato contrato = new Contrato(LocalDate.of(2024, 1, 1), (LocalDate.of(2024, 12, 31)), null, null, 0, null, null, null, null);
        assertEquals(12,contrato.calcularPeriodoVigencia()); 
    }

    @Test public void TestEquals() {
        Puesto puesto1 = new Puesto(1, 0, false, false, false, null);

        Contrato contrato1 = new Contrato(LocalDate.of(2024, 1, 1), (LocalDate.of(2024, 12, 31)), LocalDate.of(2023, 12, 31), LocalDate.of(2024, 1, 10), 10000, null, puesto1, null, null);
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 1, 1), (LocalDate.of(2024, 12, 31)), LocalDate.of(2023, 12, 31), LocalDate.of(2024, 1, 10), 10000, null, puesto1, null, null);
        assertEquals(true,contrato1.equals(contrato2));
    }

    @Test public void TestEquals2() {
        Puesto puesto1 = new Puesto(1, 0, false, false, false, null);
        Puesto puesto2 = new Puesto(2, 0, false, false, false, null);

        Contrato contrato1 = new Contrato(LocalDate.of(2024, 1, 1), (LocalDate.of(2024, 12, 31)), LocalDate.of(2023, 12, 31), LocalDate.of(2024, 1, 10), 10000, null, puesto1, null, null);
        Contrato contrato2 = new Contrato(LocalDate.of(2024, 1, 1), (LocalDate.of(2024, 12, 31)), LocalDate.of(2023, 12, 30), LocalDate.of(2024, 1, 10), 10000, null, puesto2, null, null);
        assertEquals(false,contrato1.equals(contrato2));
    }
}