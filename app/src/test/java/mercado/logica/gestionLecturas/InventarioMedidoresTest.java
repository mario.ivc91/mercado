package mercado.logica.gestionLecturas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;


import mercado.logica.gestionLecturas.exception.InventarioMedidorVacioException;
import mercado.logica.gestionLecturas.exception.MedidorExisteException;
import mercado.logica.gestionLecturas.exception.MedidorNoDisponibleException;
import mercado.logica.gestionLecturas.exception.MedidorNoExisteException;


public class InventarioMedidoresTest {
    @Test
    public void testAlmacenarMedidor() {
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");

        try {
            inventario.almacenarMedidor(medidor1);

            assertEquals(1, inventario.getInventario().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void almacenarMedidorRepetido(){
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");
        Medidor medidor2 = new Medidor("123");

        try {
            inventario.almacenarMedidor(medidor1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertThrows(MedidorExisteException.class, () -> {inventario.almacenarMedidor(medidor2);;});
    }


    @Test
    public void asignarMedidorDisponible() {
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");

        try {
            inventario.almacenarMedidor(medidor1);
    
            inventario.asignarMedidor(medidor1);

            assertFalse(medidor1.isDisponible());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void asignarMedidorNoDisponible() {
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");
        medidor1.setDisponible(false);

        try {
            inventario.almacenarMedidor(medidor1);
        } catch (MedidorExisteException e) {
            e.printStackTrace();
        }
              
        assertThrows(MedidorNoDisponibleException.class, ()->{inventario.asignarMedidor(medidor1);});

    }


    @Test
    public void listarConMedidoresDisponibles() {

        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");
        Medidor medidor2 = new Medidor("345");
        Medidor medidor3 = new Medidor("567");


        try {
            inventario.almacenarMedidor(medidor1);
            inventario.almacenarMedidor(medidor2);
            inventario.almacenarMedidor(medidor3);

            List<Medidor> listaComparar = inventario.listarMedidoresDisponibles();

            assertEquals(3, listaComparar.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void listarSinMedidoresDisponibles(){
        
        InventarioMedidores inventario= new InventarioMedidores();
        
        assertThrows(InventarioMedidorVacioException.class, ()->{inventario.listarMedidoresDisponibles();});
        
    }

    
    @Test
    public void testMedidorExiste() {

        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor2 = new Medidor("345");

        try {
            inventario.almacenarMedidor(medidor2);

            boolean existe = inventario.medidorExiste(medidor2);

            assertTrue(existe);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMedidorNoExiste(){
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor2 = new Medidor("345");

        try {
            boolean existe = inventario.medidorExiste(medidor2);

            assertFalse(existe);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void mostrarMedidorExistente(){
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");

        try {
            inventario.almacenarMedidor(medidor1);

            Medidor comparar = inventario.mostrarMedidor("123");

            assertEquals(medidor1, comparar);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void mostrarMedidorNoExistente(){
        InventarioMedidores inventario= new InventarioMedidores();


        assertThrows(MedidorNoExisteException.class, () -> {inventario.mostrarMedidor("23");});
    }


    @Test
    public void modificarMedidorExistente() {
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");
        Medidor medidor2 = new Medidor("242");

        try {
            inventario.almacenarMedidor(medidor1);

            inventario.modificarMedidor("123", "242");

            assertEquals(medidor2, inventario.mostrarMedidor("242"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Test
    public void modificarMedidorNoExistente(){
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");

        try {
            inventario.almacenarMedidor(medidor1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        assertThrows(MedidorNoExisteException.class, () -> {inventario.modificarMedidor("124", "14");;});
    }


    @Test
    public void eliminarMedidorNoExistente(){
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");
        
        assertThrows(MedidorNoExisteException.class, () -> {inventario.eliminarMedidor(medidor1);});
    }

    @Test
    public void eliminarMedidorExistente(){
        InventarioMedidores inventario= new InventarioMedidores();
        Medidor medidor1 = new Medidor("123");

        try {
            inventario.almacenarMedidor(medidor1);
            inventario.eliminarMedidor(medidor1);
            assertEquals(0, inventario.getInventario().size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
