package mercado.logica.gestionLecturas;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import java.time.LocalDate;

import org.junit.Test;


public class LecturaTest {
    
    @Test
    public void equalsFecha(){
        Lectura lectura= new Lectura(LocalDate.now(), 0);
        Lectura lectura1= new Lectura(LocalDate.now(), 0);

        boolean mismaFecha = lectura.getFecha().equals(lectura1.getFecha());

        assertTrue(mismaFecha);
    }

    @Test
    public void notEqualsFecha(){
        Lectura lectura= new Lectura(LocalDate.now(), 0);
        Lectura lectura1= new Lectura(LocalDate.of(2020, 1, 1), 0);

        boolean mismaFecha = lectura.getFecha().equals(lectura1.getFecha());

        assertFalse(mismaFecha);
    }
}
