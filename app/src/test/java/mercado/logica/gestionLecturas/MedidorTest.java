package mercado.logica.gestionLecturas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;


public class MedidorTest {
    

    @Test
    public void dosMedidorsConIgualNumSerie(){
        
        Medidor medidor1 = new Medidor("123"); 
        Medidor medidor2 = new Medidor("123"); 

        Boolean iguales= medidor1.getNumeroSerie().equals(medidor2.getNumeroSerie());

        assertTrue(iguales);
    }

    @Test
    public void dosMedidorsConDiferenteNumSerie(){
        
        Medidor medidor1 = new Medidor("123"); 
        Medidor medidor2 = new Medidor("124"); 

        Boolean iguales= medidor1.getNumeroSerie().equals(medidor2.getNumeroSerie());

        assertFalse(iguales);
    }

    @Test
    public void registrarLecturaTrue(){
        Medidor medidor1 = new Medidor("123");
        try {
            medidor1.registrarLectura(LocalDate.now(), 100);
        assertEquals(1, medidor1.getLecturas().size() );
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Test
    public void registrar2Lectura(){
    Medidor medidor1 = new Medidor("123");
        try {
            medidor1.registrarLectura(LocalDate.of(2024,05,11), 100);
            medidor1.registrarLectura(LocalDate.of(2024,05,12), 1000);

            assertEquals(2, medidor1.getLecturas().size() );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    
    @Test
    public void obtenerLecturaMasRecienteTrue(){
        Medidor medidor1 = new Medidor("123");

        try {
            medidor1.registrarLectura(LocalDate.of(2020, 1, 1), 0);
            medidor1.registrarLectura(LocalDate.of(2023,5,11), 100);
            medidor1.registrarLectura(LocalDate.now(), 1000);

            Lectura lecturaReciente=medidor1.obtenerLecturaMasReciente();

            assertEquals(LocalDate.now(), lecturaReciente.getFecha());

        } catch (Exception e) {
            e.printStackTrace();
        }
        
       
    }
    
    
    
    /// OBTENER LECTURA MAS ANTIGUA
    @Test
    public void obtenerLecturaMasAntiguaTrue(){
        Medidor medidor1 = new Medidor("123");

        try {
            medidor1.registrarLectura(LocalDate.of(2020, 1, 1), 0);
            medidor1.registrarLectura(LocalDate.now(), 100);

            Lectura lecturaReciente= medidor1.obtenerLecturaMasAntigua();

            assertEquals(LocalDate.of(2020,1,1), lecturaReciente.getFecha());

        } catch (Exception e) {
            e.printStackTrace();
        }        
    }


    @Test
    public void obtenerLecturaMasAntiguaFalse(){
        Medidor medidor1 = new Medidor("123");

        try {
            medidor1.registrarLectura(LocalDate.of(2020, 1, 1), 0);
            medidor1.registrarLectura(LocalDate.of(2022, 1, 1), 20);

            Lectura lecturaReciente = medidor1.obtenerLecturaMasAntigua();

            assertNotEquals(LocalDate.now(), lecturaReciente.getFecha());

        } catch (Exception e) {
            e.printStackTrace();
        }        
    }

    // OBTENER LECTURA DE X FECHA
    @Test
    public void obtenerLecturaDeXFechaTrue(){
        Medidor medidor1 = new Medidor("123");
        try {
            medidor1.registrarLectura(LocalDate.of(2020, 1, 1), 100);
            medidor1.registrarLectura(LocalDate.of(2022, 1, 1), 1000);

            Lectura lectura;            
            lectura = medidor1.obtenerLecturaDeXFecha(LocalDate.of(2022,1,1));
            
            assertEquals(LocalDate.of(2022, 1, 1),lectura.getFecha());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void ocuparMedidorTest(){
        Medidor medidor1 = new Medidor("123");

        try {
            medidor1.ocuparMedidor();

            assertFalse(medidor1.isDisponible());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   

}
