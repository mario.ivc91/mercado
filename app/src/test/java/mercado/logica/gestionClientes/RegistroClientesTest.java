package mercado.logica.gestionClientes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;


import mercado.logica.gestionClientes.exception.ClienteExistenteException;
import mercado.logica.gestionClientes.exception.ClienteNoExistenteException;

public class RegistroClientesTest {
    @Test
    public void modificarQuinteroExistente() {
        RegistroClientes registro = new RegistroClientes();
        Quintero quintero1 = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());

        try {
            registro.registrarCliente(quintero1);

            registro.modificarQuintero("123", "222", "ahi", "Lionel", "Messi", LocalDate.of(1986, 5, 5));
            Quintero quintero = (Quintero)registro.consultarCliente("222");

            assertEquals("222", quintero.getCuit());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void modificarQuinteroNoExistente(){
        RegistroClientes registro = new RegistroClientes();

        assertThrows(ClienteNoExistenteException.class, () -> {registro.modificarQuintero("123", "222", "alla", "lio", "messi", LocalDate.now());});
    }


    @Test
    public void modificarEmpresaExistente() {
        RegistroClientes registro = new RegistroClientes();
        Empresa empresa = new Empresa("123", "alla", "adidas");

        try {
            registro.registrarCliente(empresa);

            registro.modificarEmpresa("123", "222", "aca", "puma");

            Empresa empresa1 = (Empresa)registro.consultarCliente("222");
            assertEquals("222", empresa1.getCuit());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void modificarEmpresaNoExistente(){
        RegistroClientes registro = new RegistroClientes();

        assertThrows(ClienteNoExistenteException.class, () ->{registro.modificarEmpresa("123", "222", "aca", "messi");});
    }


    @Test
    public void consultarClienteExistente() {
        RegistroClientes registro = new RegistroClientes();
        Quintero quintero1 = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());

        try {
            registro.registrarCliente(quintero1);

            Quintero quintero = (Quintero) registro.consultarCliente("123");

            assertEquals("123", quintero.getCuit());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void consultarClienteInexistente(){
        RegistroClientes registro = new RegistroClientes();

        assertThrows(ClienteNoExistenteException.class, () ->{registro.consultarCliente("123");});
    }



    @Test
    public void testEliminarClienteExistente() {
        RegistroClientes registro = new RegistroClientes();
        Quintero quintero = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());

        try {
            registro.registrarCliente(quintero);
        
            registro.eliminarCliente("124");

            assertEquals(0, registro.getClientes().size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEliminarClienteNoExistente() {
        RegistroClientes registro = new RegistroClientes();

        assertThrows(ClienteNoExistenteException.class, () ->{registro.eliminarCliente("123");});
    }



    @Test
    public void listarConClientes() {
        RegistroClientes registro = new RegistroClientes();
        Quintero quintero = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());
        Empresa empresa = new Empresa("1234", "alla", "aidas");
        try {
            registro.registrarCliente(quintero);
            registro.registrarCliente(empresa);

            RegistroClientes lista = (RegistroClientes)registro.listarClientes();

            assertEquals(2, lista.getClientes().size());
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Test
    public void listarSinClientes() {
        RegistroClientes registro = new RegistroClientes();
        
        assertThrows(ClienteNoExistenteException.class, () -> {registro.listarClientes();});
        
        
    }



    @Test
    public void registrarClienteNoExistente() {
        RegistroClientes registro = new RegistroClientes();
        Quintero quintero = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());
        try {
            registro.registrarCliente(quintero);

            assertEquals(1, registro.getClientes().size());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void registrarClienteExiste(){
        RegistroClientes registro = new RegistroClientes();
        Quintero quintero = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());
        Quintero quintero1 = new Quintero("123", "alla", "pepe", "argento", LocalDate.now());
        try {
            registro.registrarCliente(quintero1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertThrows(ClienteExistenteException.class,() -> {registro.registrarCliente(quintero);});
    }


    @Test
    public void comprobarClienteExiste(){
        RegistroClientes registro = new RegistroClientes();
        Empresa empresa = new Empresa("123", "aca", "nike");

        try {
            registro.registrarCliente(empresa);

            boolean existe = registro.existeCliente(empresa);

            assertTrue(existe);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void comprobarClienteNoExiste(){
        RegistroClientes registro = new RegistroClientes();
        Empresa empresa = new Empresa("123", "alla", "puma");

        boolean existe = registro.existeCliente(empresa);
        
        assertFalse(existe);
    }
}
