package mercado.logica.gestionClientes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import java.time.LocalDate;
import org.junit.Test;


public class QuinteroTest {
    
    
    @Test
    public void determinarEdadCorrecta(){

        Quintero quintero = new Quintero("123", "por ahi", "pinocho", "van der sar", LocalDate.of(2000, 1, 1));

        Integer edad = quintero.edad();

        assertEquals(Integer.valueOf(24), edad);

    }

    @Test
    public void determinarEdadIncorrecta(){

        Quintero quintero = new Quintero("123", "por ahi", "pinocho", "van der sar", LocalDate.of(2000, 1, 1));

        Integer edad = quintero.edad();

        assertNotEquals(Integer.valueOf(22), edad);
    }
}
