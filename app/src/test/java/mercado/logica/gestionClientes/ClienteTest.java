package mercado.logica.gestionClientes;

import static org.junit.Assert.*;
import org.junit.Test;


public class ClienteTest {
    
    

    @Test
    public void dosClientesConIgualCuitTest(){

        Cliente quintero = new Cliente("1234", "alla");
        Cliente empresa = new Cliente("1234", "alla");

        boolean iguales = quintero.equals(empresa);

        assertTrue(iguales);

    }

    @Test
    public void dosClientesConDiferenteCuit(){
        Cliente empresa = new Cliente("123", "alla");
        Cliente empresa1 = new Cliente("1234", "ahi");

        boolean iguales = empresa.equals(empresa1);

        assertFalse(iguales);
    }

    

}
