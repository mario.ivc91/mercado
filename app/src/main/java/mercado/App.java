package mercado;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.WindowConstants;
import mercado.gui.Principal;
import mercado.logica.Mercado;
import mercado.logica.gestionLecturas.Medidor;
import mercado.logica.gestionLecturas.exception.MedidorExisteException;
import mercado.logica.gestionSectores.Excepciones.PuestoExistenteException;
import mercado.logica.gestionSectores.Excepciones.SectorExistenteException;
import mercado.logica.gestionSectores.Puesto;
import mercado.logica.gestionSectores.Sector;

public class App {
    private static Mercado baseDeDatos = Mercado.getInstance();

    public static void main(String[] args) {
        Principal ventana = new Principal(baseDeDatos);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(null);
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        
        
        
        Medidor medidor = new Medidor("000");
        try{
            baseDeDatos.getInventarioMedidor().almacenarMedidor(medidor);
        } catch (MedidorExisteException e){
            System.err.println(e.toString());
        }
        
        Sector sector = new Sector(0, "Administracion");
        try{
            baseDeDatos.getColeccionSectores().agregarSector(sector);
        } catch (SectorExistenteException e){
            System.err.println(e.toString());
        }
        
        Puesto puesto = new Puesto(0, 0, true, false, medidor);
        try{
            sector.agregarPuesto(puesto);
        } catch (PuestoExistenteException e) {
            System.out.println(e.toString());
        }
        
    }
    
    
    
}
