package mercado.logica.gestionContratos;

import java.util.ArrayList;
import java.util.List;

import mercado.logica.gestionContratos.Excepciones.ContratoExistenteException;
import mercado.logica.gestionContratos.Excepciones.ContratoInexistenteException;

public class FicheroContratos {
    private List<Contrato> contratos;

    public FicheroContratos() {
        this.contratos = new ArrayList<Contrato>();
    }

    public List<Contrato> getContratos() {
        return contratos;
    }
    public void setContratos(List<Contrato> contratos) {
        this.contratos = contratos;
    }
    //VER EXCEPCIONES
    //ADMINISTRACION PARA COLECCION DE CONTRATOS
    //VER REFACTORIZACION DE CODIGO (PATRONES COMUNES REPETIDOS)
    public void agregarContrato(Contrato contrato) throws ContratoExistenteException{
        if(contratos.size()==0){
            contratos.add(contrato);
        }
        else{
            boolean encontrado=false;
            for(Contrato c: contratos){
                if(c.equals(contrato)){
                    encontrado=true;
                    throw new ContratoExistenteException("EL CONTRATO YA EXISTE "+contrato.toString());
                }
            }
            if(encontrado==false){
                contratos.add(contrato);
            }
        }
    }

    public void eliminarContrato(Contrato contrato) throws ContratoInexistenteException{
        boolean encontrado=false;
        for(Contrato c: contratos){
            if(c.equals(contrato)){
                encontrado=true;
                contratos.remove(c);
                break;
            }
        }
        if(encontrado==false){
            throw new ContratoInexistenteException("EL CONTRATO NO EXISTE");
        }
    }
    //FUNCION DEBE DEVOLVER DATO TIPO OBJETO? (CONTRATO)
    public void buscarContrato(Contrato contrato) throws ContratoInexistenteException{
        boolean encontrado=false;
        for(Contrato c: contratos){
            if(c.equals(contrato)){
                System.out.println("EL CONTRATO ES:" + contrato.toString());
                encontrado=true;
                break;
            }
        }
        if(encontrado==false){
            throw new ContratoInexistenteException("EL CONTRATO NO EXISTE");
        }
    }
    //VER METODO PARA MODIFICAR
    public void modificarContrato(Contrato contrato) throws ContratoInexistenteException{
        boolean encontrado=false;
        for(Contrato c: contratos){
            if(c.equals(contrato)){
                contratos.remove(c);
                contratos.add(contrato);
                encontrado=true;
                break;
            }
        }
        if(encontrado==false){
            throw new ContratoInexistenteException("EL CONTRATO NO EXISTE");
        }
    }
}