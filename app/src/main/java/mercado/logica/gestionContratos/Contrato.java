package mercado.logica.gestionContratos;

import java.time.LocalDate;
import java.time.Period;

import mercado.logica.gestionClientes.Cliente;
import mercado.logica.gestionLecturas.Lectura;
import mercado.logica.gestionSectores.Puesto;

public class Contrato {
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private LocalDate fechaInstrumento;
    private LocalDate fechaVencimientoPago;
    private float precio;
    private ResponsableMercado responsableMercado;
    private Puesto puesto;
    private Cliente cliente;
    private Lectura lectura;

    public Contrato(LocalDate fechaInicio, LocalDate fechaFin, LocalDate fechaInstrumento,
        LocalDate fechaVencimientoPago, float precio, ResponsableMercado responsableMercado, 
        Puesto puesto, Cliente cliente, Lectura lectura) {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaInstrumento = fechaInstrumento;
        this.fechaVencimientoPago = fechaVencimientoPago;
        this.precio = precio;
        this.responsableMercado = responsableMercado;
        this.puesto = puesto;
        this.cliente = cliente;
        this.lectura = lectura;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }
    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }
    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public LocalDate getFechaInstrumento() {
        return fechaInstrumento;
    }
    public void setFechaInstrumento(LocalDate fechaInstrumento) {
        this.fechaInstrumento = fechaInstrumento;
    }

    public LocalDate getFechaVencimientoPago() {
        return fechaVencimientoPago;
    }
    public void setFechaVencimientoPago(LocalDate fechaVencimientoPago) {
        this.fechaVencimientoPago = fechaVencimientoPago;
    }

    public float getPrecio() {
        return precio;
    }
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public ResponsableMercado getResponsableMercado() {
        return responsableMercado;
    }
    public void setResponsableMercado(ResponsableMercado responsableMercado) {
        this.responsableMercado = responsableMercado;
    }

    public Puesto getPuesto() {
        return puesto;
    }
    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Lectura getLectura() {
        return lectura;
    }

    public void setLectura(Lectura lectura) {
        this.lectura = lectura;
    }
    
    @Override
    public boolean equals(Object obj) {
        Contrato contrato = (Contrato) obj;
        return fechaInstrumento.equals(contrato.getFechaInstrumento()) && puesto.getCodigo()==contrato.getPuesto().getCodigo();
    }
    
    //VER METODO PARA MEJORAR TEST (TOMAR DIAS/30 = (INT) MESES)
    public int calcularPeriodoVigencia(){
        Period periodo = Period.between(fechaInicio,fechaFin);
        return periodo.getMonths()+1;
    }

}