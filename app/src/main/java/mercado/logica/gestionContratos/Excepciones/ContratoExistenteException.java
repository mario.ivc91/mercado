package mercado.logica.gestionContratos.Excepciones;

public class ContratoExistenteException extends Exception {
    public ContratoExistenteException(String mensaje){
        super(mensaje);
    }
}