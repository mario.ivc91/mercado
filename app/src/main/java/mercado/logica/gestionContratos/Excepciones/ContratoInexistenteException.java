package mercado.logica.gestionContratos.Excepciones;

public class ContratoInexistenteException extends Exception {
    public ContratoInexistenteException(String mensaje){
        super(mensaje);
    }
}