package mercado.logica.gestionSectores;

import java.util.ArrayList;
import java.util.List;

import mercado.logica.gestionSectores.Excepciones.PuestoExistenteException;
import mercado.logica.gestionSectores.Excepciones.PuestoInexistenteException;

public class Sector {
    private int codigo;
    private String nombre;
    private List<Puesto> puestos = new ArrayList<Puesto>();
    
    public Sector(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Puesto> getPuestos() {
        return puestos;
    }
    public void setPuestos(ArrayList<Puesto> puestos) {
        this.puestos = puestos;
    }

    @Override
    public boolean equals(Object obj) {
        Sector sector = (Sector) obj;
        return nombre.equals(sector.getNombre()) || codigo==sector.getCodigo();
    }
    //CADA ADMINISTRACION DE COLECCIONES CON SUS METODOS, TESTS Y EXCEPCIONES PERSONALIZADAS
    public void agregarPuesto(Puesto puesto) throws PuestoExistenteException{
        if(puestos.size()==0){
            puestos.add(puesto);
        }
        else{
            boolean encontrado=false;
            for(Puesto p: puestos){
                if(p.equals(puesto)){
                    encontrado=true;
                    throw new PuestoExistenteException("EL PUESTO YA EXISTE"+p.toString());
                }
            }
            if(encontrado==false){
                puestos.add(puesto);
            }
        }
    }

    public void eliminarPuesto(Puesto puesto) throws PuestoInexistenteException{
        boolean encontrado=false;
        for(Puesto p: puestos){
            if(p.equals(puesto)){
                encontrado=true;
                puestos.remove(p);
                break;
            }
        }
        if(encontrado==false){
            throw new PuestoInexistenteException("EL PUESTO NO EXISTE");
        }
    }

    public void buscarPuesto(Puesto puesto) throws PuestoInexistenteException{
        boolean encontrado=false;
        for(Puesto p: puestos){
            if(p.equals(puesto)){
                System.out.println("EL PUESTO ES:");
                System.out.println(p.toString());
                encontrado=true;
                break;
            }
        }
        if(encontrado==false){
            throw new PuestoInexistenteException("EL PUESTO NO EXISTE");
        }
    }
    //VER METODO PARA MODIFICAR
    public void modificarPuesto(Puesto puesto) throws PuestoInexistenteException{
        boolean encontrado=false;
        for(Puesto p: puestos){
            if(p.equals(puesto)){
                puestos.remove(p);
                puestos.add(puesto);
                encontrado=true;
                break;
            }
        }
        if(encontrado==false){
            throw new PuestoInexistenteException("EL PUESTO NO EXISTE");
        }
    }
    //METODO PARA CONSULTAR PUESTOS DISPONIBLES
}