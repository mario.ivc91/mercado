package mercado.logica.gestionSectores;

import mercado.logica.gestionLecturas.Medidor;
//VER CARACTERISTICAS DEL PUESTO (BOOLEAN) COMO ARRAYLIST DE COMPONENTES ADICIONALES
public class Puesto {
    private int codigo;
    private int superficie;
    private boolean disponibilidad;
    private boolean techo;
    private boolean camaraRefrigerante;
    private Medidor medidor;
    
    public Puesto(int codigo, int superficie, boolean techo, 
        boolean camaraRefrigerante, Medidor medidor) {
        this.codigo = codigo;
        this.superficie = superficie;
        this.disponibilidad = true;
        this.techo = techo;
        this.camaraRefrigerante = camaraRefrigerante;
        this.medidor = medidor;
    }

    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getSuperficie() {
        return superficie;
    }
    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }

    public boolean isDisponibilidad() {
        return disponibilidad;
    }
    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public boolean isTecho() {
        return techo;
    }
    public void setTecho(boolean techo) {
        this.techo = techo;
    }

    public boolean isCamaraRefrigerante() {
        return camaraRefrigerante;
    }
    public void setCamaraRefrigerante(boolean camaraRefrigerante) {
        this.camaraRefrigerante = camaraRefrigerante;
    }

    public Medidor getMedidor() {
        return medidor;
    }
    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }

    @Override
    public boolean equals(Object obj) {
        Puesto puesto = (Puesto) obj;
        return codigo == puesto.getCodigo();
    }
}