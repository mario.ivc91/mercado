package mercado.logica.gestionSectores.Excepciones;

public class PuestoExistenteException extends Exception {
    public PuestoExistenteException(String mensaje){
        super(mensaje);
    }
}