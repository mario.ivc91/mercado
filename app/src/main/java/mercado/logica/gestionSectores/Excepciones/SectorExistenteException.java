package mercado.logica.gestionSectores.Excepciones;

public class SectorExistenteException extends Exception {
    public SectorExistenteException(String mensaje){
        super(mensaje);
    }
}