package mercado.logica.gestionSectores.Excepciones;

public class PuestoInexistenteException extends Exception {
    public PuestoInexistenteException(String mensaje){
        super(mensaje);
    }
}