package mercado.logica.gestionSectores.Excepciones;

public class SectorInexistenteException extends Exception {
    public SectorInexistenteException(String mensaje){
        super(mensaje);
    }
}