package mercado.logica.gestionSectores;

import java.util.ArrayList;
import java.util.List;

import mercado.logica.gestionSectores.Excepciones.SectorExistenteException;
import mercado.logica.gestionSectores.Excepciones.SectorInexistenteException;

public class ColeccionSectores {
    private List<Sector> sectores = new ArrayList<Sector>();;

    public ColeccionSectores() {
    }

    public List<Sector> getSectores() {
        return sectores;
    }
    public void setSectores(List<Sector> sectores) {
        this.sectores = sectores;
    }
    
    //ADMINISTRACION COLECCION DE SECTORES
    public void agregarSector(Sector sector) throws SectorExistenteException{
        boolean encontrado=false;
        for(Sector s: sectores){
            if(s.equals(sector)){
                encontrado=true;
                throw new SectorExistenteException("EL SECTOR YA EXISTE");
            }
        }
        if(encontrado==false){
            sectores.add(sector);
        }
    }

    public void eliminarSector(Sector sector) throws SectorInexistenteException{
        boolean encontrado=false;
        for(Sector s: sectores){
            if(s.equals(sector)){
                encontrado=true;
                sectores.remove(sector);
                break;
            }
        }
        if(encontrado==false){
            throw new SectorInexistenteException("EL SECTOR NO EXISTE");
        }
    }

    public void buscarSector(Sector sector) throws SectorInexistenteException{
        boolean encontrado=false;
        for(Sector s: sectores){
            if(s.equals(sector)){
                System.out.println("EL SECTOR ES:");
                System.out.println(s.toString());
                encontrado=true;
                break;
            }
        }
        if(encontrado==false){
            throw new SectorInexistenteException("EL SECTOR NO EXISTE");
        }
    }
    
    //Metodo para acceder a un sector
    public Sector recuperarSector(int i){
        return sectores.get(i);
    }
    
    //VER METODO PARA MODIFICAR
    public void modificarSector(Sector sector) throws SectorInexistenteException{
        boolean encontrado=false;
        for(Sector s: sectores){
            if(s.equals(sector)){
                sectores.remove(s);
                sectores.add(sector);
                encontrado=true;
                break;
            }
        }
        if(encontrado==false){
            throw new SectorInexistenteException("EL SECTOR NO EXISTE");
        }
    }
}