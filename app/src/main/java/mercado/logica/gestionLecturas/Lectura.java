package mercado.logica.gestionLecturas;

import java.time.LocalDate;

public class Lectura {
    private LocalDate fecha;
    private double consumo;
    
    public Lectura(LocalDate fecha, double consumo) {
        this.fecha = fecha;
        this.consumo = consumo;
    }
    
    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getConsumo() {
        return consumo;
    }

    public void setConsumo(double consumo) {
        this.consumo = consumo;
    }

    
    @Override
    public boolean equals(Object obj) {
        Lectura compara = (Lectura) obj;
        return this.getFecha().equals(compara.getFecha()) ;
    }

}
