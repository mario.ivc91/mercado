package mercado.logica.gestionLecturas.exception;

public class MedidorNoDisponibleException extends Exception{
    public MedidorNoDisponibleException(String mensaje){
        super(mensaje);
    }
}
