package mercado.logica.gestionLecturas.exception;

public class MedidorExisteException extends Exception{
    public MedidorExisteException(String mensaje){
        super(mensaje);
    }
}
