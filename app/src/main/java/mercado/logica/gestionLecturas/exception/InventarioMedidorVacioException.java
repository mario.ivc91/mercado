package mercado.logica.gestionLecturas.exception;

public class InventarioMedidorVacioException extends Exception {
    public InventarioMedidorVacioException(String mensaje){
        super(mensaje);
    }
}
