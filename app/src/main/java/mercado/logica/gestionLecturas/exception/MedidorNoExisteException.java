package mercado.logica.gestionLecturas.exception;

public class MedidorNoExisteException extends Exception {
    public MedidorNoExisteException(String mensaje){
        super(mensaje);
    }
}
