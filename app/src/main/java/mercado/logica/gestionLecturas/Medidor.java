package mercado.logica.gestionLecturas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Medidor {
    private String numeroSerie;
    private boolean disponible;
    private List<Lectura> lecturas = new ArrayList<>();
    
    public Medidor(String numeroSerie){
        this.numeroSerie = numeroSerie;
        this.disponible = true;
    }
    
    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }
    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }


    public void registrarLectura(LocalDate fecha, double consumo) throws Exception {
        Lectura lectura = new Lectura (fecha,  consumo);
        Lectura ultimaLectura = obtenerLecturaMasReciente();
        if( ultimaLectura == null || ultimaLectura.getFecha().isBefore(fecha)){
            if ( ultimaLectura == null || ultimaLectura.getConsumo()< consumo) {
                this.lecturas.add(lectura);
                return;
            }
            throw new Exception("El consumo no puede ser inferior al anterior");
        }
        throw new Exception("La fecha debe ser posterior a la ultima lectura");
    }

    public int getNumeroDeLecturas() {
        return lecturas.size();
    }
    
    @Override
    public boolean equals(Object obj) {
        Medidor aComparar = (Medidor)obj;
        return this.getNumeroSerie().equals(aComparar.getNumeroSerie());
    }

    public Lectura obtenerLecturaDeXFecha(LocalDate fecha) throws Exception{
        for (Lectura lectura : lecturas) {
            if (lectura.getFecha().equals(fecha)) {
                return lectura;
            }
        }
        throw new Exception("No se encontró ninguna lectura para la fecha dada");
    }

    public Lectura obtenerLecturaMasReciente(){
        LocalDate fechaMasReciente = LocalDate.MIN;
        Lectura lecturaReciente = null;
        for (Lectura lectura : lecturas) {
            if (lectura.getFecha().isAfter(fechaMasReciente)) {
                fechaMasReciente=lectura.getFecha();
                lecturaReciente = lectura;
            }
        }             
        return lecturaReciente;   
    }


    public Lectura obtenerLecturaMasAntigua() throws Exception{
        LocalDate fechaMasAntigua = LocalDate.MAX;
        Lectura lecturaAntigua = null;
        if (lecturas.size()!=0) {
            for (Lectura lectura : lecturas) {
                if (lectura.getFecha().isBefore(fechaMasAntigua)) {
                    fechaMasAntigua=lectura.getFecha();
                    lecturaAntigua = lectura;
                }
            }
            return lecturaAntigua;    
        }
        throw new Exception("El medidor no posee lecturas registradas");       
    }

    public List<Lectura> getLecturas() {
        return lecturas;
    }    

    public void ocuparMedidor(){
        setDisponible(false);;
    }

    @Override
    public String toString() {
        return "Medidor [Numero de serie:" + numeroSerie + 
        ", disponibilidad: " + disponible + "]";
    }

    
   
}
