package mercado.logica.gestionLecturas;

import java.util.ArrayList;
import java.util.List;

import mercado.logica.gestionLecturas.exception.*;


public class InventarioMedidores {
    private List<Medidor>inventario = new ArrayList<>();
    
    public InventarioMedidores(){}

    public void almacenarMedidor(Medidor medidor) throws MedidorExisteException{
        boolean existe=medidorExiste(medidor);
        if (existe==false) {
            inventario.add(medidor);
        }else throw new MedidorExisteException("Ya fue registrado un medidor con este numero de serie");
    }

    public boolean medidorExiste(Medidor medidor) {
        if(inventario.contains(medidor)){
            return true;
        }else
            return false;
    }

    public List<Medidor> listarMedidoresDisponibles(){
        List<Medidor> medidoresDisponibles = new ArrayList<>();
        for (Medidor medidor : inventario) {
            if (medidor.isDisponible() == true) {
                medidoresDisponibles.add(medidor);
            }                
        } 
        return medidoresDisponibles;
    }

    public void asignarMedidor(Medidor medidorAsignar) throws MedidorNoDisponibleException, MedidorNoExisteException {
        for (Medidor medidor : inventario) {
            if (medidor.equals(medidorAsignar)) {
                if (medidor.isDisponible()==true) {
                    medidor.ocuparMedidor();
                    return;
                }else throw new MedidorNoDisponibleException("El medidor no esta disponible");
            } 
        }throw new MedidorNoExisteException("No existe un medidor registrado con el numero de serie dado");
    }

    public List<Medidor> getInventario() {
        return inventario;
    }

    public void eliminarMedidor(Medidor medidor) throws MedidorNoExisteException{
        boolean existe = inventario.remove(medidor);
        if (!existe) {
            throw new MedidorNoExisteException("Este medidor no esta registrado");
        }
    }

    public void modificarMedidor(String serieActual, String serieNueva)throws MedidorNoExisteException{
        for (Medidor medidor : inventario) {
            if (medidor.getNumeroSerie().equals(serieActual)) {
                medidor.setNumeroSerie(serieNueva);
                return;
            }
        } 
        throw new MedidorNoExisteException("No existe el medidor que desea modificar");
    }
    
    public Medidor mostrarMedidor(String numeroSerie) throws MedidorNoExisteException{
        for (Medidor medidor : inventario) {
            if (medidor.getNumeroSerie().equals(numeroSerie)) {
                return medidor;
            }
        } 
        throw new MedidorNoExisteException("No existe el medidor que desea mostrar");
    }
}
