package mercado.logica;

import mercado.logica.gestionClientes.RegistroClientes;
import mercado.logica.gestionContratos.FicheroContratos;
import mercado.logica.gestionLecturas.InventarioMedidores;
import mercado.logica.gestionSectores.ColeccionSectores;

public class Mercado {
    private static Mercado instance;
    
    private RegistroClientes clientes = new RegistroClientes();
    private FicheroContratos contratos = new FicheroContratos();
    private InventarioMedidores medidores = new InventarioMedidores();
    private ColeccionSectores sectores = new ColeccionSectores();
    
    private Mercado(){}
    
    public static Mercado getInstance(){
        if (instance == null) {
            instance = new Mercado();
        }
        return instance;
    }
    
    public RegistroClientes getRegistroClientes(){
        return clientes;
    }
    
    public FicheroContratos getFicheroContrato(){
        return contratos;
    }
    
    public InventarioMedidores getInventarioMedidor(){
        return medidores;
    }
    
    public ColeccionSectores getColeccionSectores(){
        return sectores;
    }
    
}