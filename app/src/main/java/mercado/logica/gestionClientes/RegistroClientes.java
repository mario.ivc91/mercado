package mercado.logica.gestionClientes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import mercado.logica.gestionClientes.exception.ClienteExistenteException;
import mercado.logica.gestionClientes.exception.ClienteNoExistenteException;



public class RegistroClientes {
    private List<Cliente> clientes = new ArrayList<>();


    public void registrarCliente(Cliente clienteNuevo) throws ClienteExistenteException {  
        boolean existe = existeCliente(clienteNuevo);
        if (existe == false) {
            clientes.add(clienteNuevo); 
            return;
        } 
        throw new ClienteExistenteException("Ya existe un cliente registrado con este cuit");
    }

    public boolean existeCliente (Cliente nuevo){
        boolean existe = false;
        for (Cliente cliente : clientes) {
            existe = cliente.equals(nuevo);
            if (existe==true) {
                return existe;
            }
        }        
        return existe;
    }

    public void eliminarCliente(String cuit) throws ClienteNoExistenteException{

        for (Cliente cliente : clientes) {
            if (cliente.getCuit().equals(cuit)) {
                clientes.remove(cliente);
                return;
            }
        }
        throw new ClienteNoExistenteException("El cuit dado no esta registrado"); 
    }

    public List<Cliente> listarClientes() throws ClienteNoExistenteException{
        if (!clientes.isEmpty()) {
            return clientes;
        }
        throw new ClienteNoExistenteException("No existen clientes para mostrar");
    }

    public void modificarQuintero(String cuitActual, String cuitNuevo, String direccion, String nombre, String apellido, LocalDate fechaNacimiento ) throws ClienteNoExistenteException{
        Quintero quintero = (Quintero)consultarCliente(cuitActual);
            quintero.setCuit(cuitNuevo);
            quintero.setDireccion(direccion);
            quintero.setNombre(nombre);
            quintero.setApellido(apellido);
            quintero.setFechaNacimiento(fechaNacimiento);
    }
    
    public void modificarEmpresa(String cuitActual, String cuitNuevo, String direccion, String razonsocial) throws ClienteNoExistenteException{
        Empresa empresa = (Empresa)consultarCliente(cuitActual);
        empresa.setCuit(cuitNuevo);
        empresa.setDireccion(direccion);
        empresa.setRazonSocial(razonsocial);
        return;
    }

    public Cliente consultarCliente(String cuit) throws ClienteNoExistenteException{
        for (Cliente cliente : clientes) {
            if (cliente.getCuit().equals(cuit)) {
                return cliente;
            }
        }
        throw new ClienteNoExistenteException("No existe un cliente con el cuit dado");
    }

    public List<Cliente> getClientes() {
        return clientes;
    }



}
