package mercado.logica.gestionClientes;

import java.time.LocalDate;
import java.time.Period;

public class Quintero extends Cliente{
    private String nombre;
    private String apellido;
    private LocalDate fechaNacimiento;
 
    public Quintero(String cuit, String direccion, String nombre, String apellido,
            LocalDate fechaNacimiento) {
        super(cuit, direccion);
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacimiento = fechaNacimiento;
    }
   
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Nombre: " + this.getNombre() +
             ", apellido: " + this.getApellido() + 
             ", cuit: " + this.getCuit() + 
             ", fechaNacimiento: " + this.getFechaNacimiento();
    }
    

    public Integer edad(){
        Period periodo = Period.between(getFechaNacimiento(), LocalDate.now());
        return periodo.getYears();
    }
    
}