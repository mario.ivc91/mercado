package mercado.logica.gestionClientes;


public class Cliente {
    private String cuit;
    private String direccion;
    
    public Cliente(String cuit, String direccion) {
        this.cuit = cuit;
        this.direccion = direccion;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public boolean equals(Object obj) {
        Cliente clienteComparar = (Cliente)obj;
        return getCuit().equals(clienteComparar.getCuit()) ;
    }
}
