package mercado.logica.gestionClientes.exception;

public class ClienteExistenteException extends Exception{
    public ClienteExistenteException(String mensaje){
        super(mensaje);
    }
}
