package mercado.logica.gestionClientes.exception;

public class ClienteNoExistenteException extends Exception{

	public ClienteNoExistenteException(String mensaje) {
        super(mensaje);
	}
    
}
