package mercado.logica.gestionClientes;

public class Empresa extends Cliente{
    
    private String razonSocial;

    public Empresa(String cuit, String direccion, String razonSocial) {
        super(cuit, direccion);
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Override
    public String toString() {
        return "Empresa: " + razonSocial + ", cuit: " + this.getCuit();
    }

    
}
