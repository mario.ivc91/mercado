
package mercado.gui.menuConsulta.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import mercado.logica.gestionSectores.Sector;

/**
 *
 * @author AlexDev
 */
public class SectorTableModel extends AbstractTableModel{
    private List<Sector> sectores;

    public SectorTableModel(List<Sector> sectores) {
        this.sectores = sectores;
    }
    
    
    @Override
    public int getRowCount() {
        return sectores.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Sector sector = sectores.get(rowIndex);

        
        switch (columnIndex) {
            case 0:
                return sector.getCodigo();
            case 1:
                return sector.getNombre();
            case 2:
                return sector.getPuestos().size();            
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Codigo";
            case 1:
                return "Nombre";
            case 2:
                return "Num Puestos";
            default:
                return null;
        }
    }
    
    
}
