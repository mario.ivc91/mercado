package mercado.gui.menuRegistrar;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import mercado.logica.Mercado;
import mercado.logica.auxiliar.ValidarInput;
import mercado.logica.gestionLecturas.Medidor;
import mercado.logica.gestionSectores.Excepciones.PuestoExistenteException;
import mercado.logica.gestionSectores.Puesto;
import mercado.logica.gestionSectores.Sector;

public class RegistrarPuesto extends javax.swing.JFrame {
    private Mercado baseDeDatos;
   
    public RegistrarPuesto(Mercado baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
        cargarComboSector();
        cargarComboMedidores();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtCodPuesto = new javax.swing.JTextField();
        txtSupPuesto = new javax.swing.JTextField();
        cbTecho = new javax.swing.JCheckBox();
        cbCamaraRefri = new javax.swing.JCheckBox();
        cmbMedidor = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        cmbSector = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1.setText("Nuevo Puesto");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("Codigo");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("Superficie");

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel6.setText("Medidor");

        txtCodPuesto.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtSupPuesto.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        cbTecho.setText("TECHO");

        cbCamaraRefri.setText("CAMARA REFRIGERANTE");

        cmbMedidor.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cmbMedidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMedidorActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setText("Caracteristicas");

        btnAceptar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnAceptar.setText("ACEPTAR");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnCancel.setText("CANCELAR");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("Sector");

        cmbSector.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cmbSector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSectorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(198, 198, 198))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(125, 125, 125)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbTecho)
                            .addComponent(cbCamaraRefri))
                        .addGap(95, 95, 95))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(80, 80, 80)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCodPuesto, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(txtSupPuesto))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(47, 47, 47)
                                .addComponent(btnCancel))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel4))
                                .addGap(78, 78, 78)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cmbMedidor, 0, 120, Short.MAX_VALUE)
                                    .addComponent(cmbSector, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jLabel1)
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCodPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtSupPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbTecho)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbCamaraRefri)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cmbMedidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbSector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancel))
                .addGap(46, 46, 46))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void cmbSectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSectorActionPerformed
        
    }//GEN-LAST:event_cmbSectorActionPerformed

    private void cmbMedidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMedidorActionPerformed
    }//GEN-LAST:event_cmbMedidorActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        String codigo, superficie;
        boolean techo, camaraRefrigerante;
        int cod, sup;
        
        codigo = txtCodPuesto.getText();
        superficie = txtSupPuesto.getText();
        techo = cbTecho.isSelected();
        camaraRefrigerante = cbCamaraRefri.isSelected();
        Sector sector = (Sector) cmbSector.getSelectedItem();
        Medidor medidor = (Medidor) cmbMedidor.getSelectedItem();
        
        try{
            txtCodPuesto.setInputVerifier(new ValidarInput());
            txtSupPuesto.setInputVerifier(new ValidarInput());
            cod = Integer.parseInt(codigo);
            sup = Integer.parseInt(superficie);            
        } catch (NumberFormatException e){
            JOptionPane.showMessageDialog(this, e.toString(), "El codigo y superficie debe ser caracter numerico", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try{
            if(cmbMedidor.getSelectedIndex() != 0){
            }            
        }catch (IllegalArgumentException e){
            JOptionPane.showMessageDialog(this, e.toString(), "Debe seleccionar un Medidor", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try{
            if(cmbSector.getSelectedIndex()!= 0){
            }            
        }catch (IllegalArgumentException e){
            JOptionPane.showMessageDialog(this, e.toString(), "Debe seleccionar un puesto", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try{
            Puesto puesto = new Puesto(cod, sup, techo, camaraRefrigerante, medidor);
            baseDeDatos.getColeccionSectores().recuperarSector(cmbSector.getSelectedIndex()).agregarPuesto(puesto);
            JOptionPane.showMessageDialog(this, "Puesto Registrado!");
            dispose(); 
        } catch(PuestoExistenteException e){
            JOptionPane.showMessageDialog(this, e.toString(), "El codigo de puesto ya esta registrado", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
    }//GEN-LAST:event_btnAceptarActionPerformed
    
    private void cargarComboSector(){
        for (Sector sector : baseDeDatos.getColeccionSectores().getSectores())
            cmbSector.addItem(sector.getNombre());
    }
    
    private void cargarComboMedidores(){
        List<Medidor> medidores = baseDeDatos.getInventarioMedidor().listarMedidoresDisponibles();
        for (Medidor medidor : medidores)
            cmbMedidor.addItem(medidor.getNumeroSerie());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancel;
    private javax.swing.JCheckBox cbCamaraRefri;
    private javax.swing.JCheckBox cbTecho;
    private javax.swing.JComboBox<String> cmbMedidor;
    private javax.swing.JComboBox<String> cmbSector;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCodPuesto;
    private javax.swing.JTextField txtSupPuesto;
    // End of variables declaration//GEN-END:variables
}