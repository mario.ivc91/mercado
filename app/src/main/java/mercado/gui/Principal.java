package mercado.gui;

import mercado.gui.menuBaja.*;
import mercado.gui.menuConsulta.ConsultarCliente;
import mercado.gui.menuConsulta.ConsultarPuesto;
import mercado.gui.menuConsulta.ConsultarSector;
import mercado.gui.menuRegistrar.*;
import mercado.logica.Mercado;

public class Principal extends javax.swing.JFrame {
    private Mercado baseDeDatos;

    public Principal(Mercado baseDeDatos){
        this.baseDeDatos = baseDeDatos;
        initComponents();
        
    }
    public Principal() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnRegistrarAdmin = new javax.swing.JButton();
        btnLogin = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenuItemRegistrarQuintero = new javax.swing.JMenuItem();
        jMenuItemRegistrarEmpresa = new javax.swing.JMenuItem();
        jMenuRegistrarSector = new javax.swing.JMenuItem();
        jMenuRegMedidor = new javax.swing.JMenuItem();
        jMenuRegistrarPuesto = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuConsultarCliente = new javax.swing.JMenuItem();
        menuConsutlarPuesto = new javax.swing.JMenuItem();
        menuConsultarSector = new javax.swing.JMenuItem();
        menuConsultarMedidor = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItemModificarCliente = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuCliente = new javax.swing.JMenuItem();
        jMenuMedidor = new javax.swing.JMenuItem();
        jMenuPuesto = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuNuevoContrato = new javax.swing.JMenuItem();
        jMenuBuscarContrato = new javax.swing.JMenuItem();
        jMenuListarContratos = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnRegistrarAdmin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnRegistrarAdmin.setText("REGISTRAR");

        btnLogin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnLogin.setText("INGRESAR");

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\AlexDev\\Desktop\\DiseñoII\\mercado\\app\\src\\main\\java\\mercado\\gui\\resources\\mercado.png")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
                .addComponent(btnRegistrarAdmin)
                .addGap(99, 99, 99))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(126, 126, 126))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLogin)
                    .addComponent(btnRegistrarAdmin))
                .addGap(68, 68, 68))
        );

        jMenu.setText("Registrar");

        jMenu7.setText("Cliente");

        jMenuItemRegistrarQuintero.setText("Quintero");
        jMenuItemRegistrarQuintero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRegistrarQuinteroActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItemRegistrarQuintero);

        jMenuItemRegistrarEmpresa.setText("Empresa");
        jMenuItemRegistrarEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRegistrarEmpresaActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItemRegistrarEmpresa);

        jMenu.add(jMenu7);

        jMenuRegistrarSector.setText("Sector");
        jMenuRegistrarSector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuRegistrarSectorActionPerformed(evt);
            }
        });
        jMenu.add(jMenuRegistrarSector);

        jMenuRegMedidor.setText("Medidor");
        jMenuRegMedidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuRegMedidorActionPerformed(evt);
            }
        });
        jMenu.add(jMenuRegMedidor);

        jMenuRegistrarPuesto.setText("Puesto");
        jMenuRegistrarPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuRegistrarPuestoActionPerformed(evt);
            }
        });
        jMenu.add(jMenuRegistrarPuesto);

        jMenuBar1.add(jMenu);

        jMenu2.setText("Consultar");

        menuConsultarCliente.setText("Cliente");
        menuConsultarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultarClienteActionPerformed(evt);
            }
        });
        jMenu2.add(menuConsultarCliente);

        menuConsutlarPuesto.setText("Puesto");
        menuConsutlarPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsutlarPuestoActionPerformed(evt);
            }
        });
        jMenu2.add(menuConsutlarPuesto);

        menuConsultarSector.setText("Sector");
        menuConsultarSector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultarSectorActionPerformed(evt);
            }
        });
        jMenu2.add(menuConsultarSector);

        menuConsultarMedidor.setText("Medidor");
        jMenu2.add(menuConsultarMedidor);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Modificar");

        jMenuItemModificarCliente.setText("Cliente");
        jMenu3.add(jMenuItemModificarCliente);

        jMenuItem1.setText("Puesto");
        jMenu3.add(jMenuItem1);

        jMenuItem2.setText("Sector");
        jMenu3.add(jMenuItem2);

        jMenuItem3.setText("Medidor");
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Baja");

        jMenuCliente.setText("Cliente");
        jMenuCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuClienteActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuCliente);

        jMenuMedidor.setText("Medidor");
        jMenuMedidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuMedidorActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuMedidor);

        jMenuPuesto.setText("Puesto");
        jMenuPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPuestoActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuPuesto);

        jMenuBar1.add(jMenu4);

        jMenu5.setText("Contrato");

        jMenuNuevoContrato.setText("Nuevo Contrato");
        jMenu5.add(jMenuNuevoContrato);

        jMenuBuscarContrato.setText("Buscar Contrato");
        jMenu5.add(jMenuBuscarContrato);

        jMenuListarContratos.setText("Listar Contratos");
        jMenu5.add(jMenuListarContratos);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemRegistrarEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRegistrarEmpresaActionPerformed
        RegistrarEmpresa empresa = new RegistrarEmpresa(baseDeDatos);
        empresa.setVisible(true);
        empresa.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItemRegistrarEmpresaActionPerformed

    private void jMenuItemRegistrarQuinteroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRegistrarQuinteroActionPerformed
        RegistrarQuintero quintero = new RegistrarQuintero(baseDeDatos);
        quintero.setVisible(true);
        quintero.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItemRegistrarQuinteroActionPerformed

    private void jMenuRegistrarSectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuRegistrarSectorActionPerformed
        RegistrarSector sector = new RegistrarSector(baseDeDatos);
        sector.setVisible(true);
        sector.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuRegistrarSectorActionPerformed

    private void jMenuRegMedidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuRegMedidorActionPerformed
        RegistrarMedidor medidor = new RegistrarMedidor(baseDeDatos);
        medidor.setVisible(true);
        medidor.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuRegMedidorActionPerformed

    private void jMenuRegistrarPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuRegistrarPuestoActionPerformed
        RegistrarPuesto puesto = new RegistrarPuesto(baseDeDatos);
        puesto.setVisible(true);
        puesto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuRegistrarPuestoActionPerformed

    private void jMenuClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuClienteActionPerformed
        BajaCliente cliente = new BajaCliente();
        cliente.setVisible(true);
        cliente.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuClienteActionPerformed

    private void jMenuMedidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuMedidorActionPerformed
        BajaMedidor medidor = new BajaMedidor();
        medidor.setVisible(true);
        medidor.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuMedidorActionPerformed

    private void jMenuPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPuestoActionPerformed
        BajaPuesto puesto = new BajaPuesto();
        puesto.setVisible(true);
        puesto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuPuestoActionPerformed

    private void menuConsultarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultarClienteActionPerformed
        ConsultarCliente consulta = new ConsultarCliente(baseDeDatos);
        consulta.setVisible(true);
        consulta.setLocationRelativeTo(null);
    }//GEN-LAST:event_menuConsultarClienteActionPerformed

    private void menuConsutlarPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsutlarPuestoActionPerformed
        ConsultarPuesto puesto = new ConsultarPuesto(baseDeDatos);
        puesto.setVisible(true);
        puesto.setLocationRelativeTo(null);
    }//GEN-LAST:event_menuConsutlarPuestoActionPerformed

    private void menuConsultarSectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultarSectorActionPerformed
        ConsultarSector sector = new ConsultarSector(baseDeDatos);
        sector.setVisible(true);
        sector.setLocationRelativeTo(null);
        pack();
    }//GEN-LAST:event_menuConsultarSectorActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnRegistrarAdmin;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuBuscarContrato;
    private javax.swing.JMenuItem jMenuCliente;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItemModificarCliente;
    private javax.swing.JMenuItem jMenuItemRegistrarEmpresa;
    private javax.swing.JMenuItem jMenuItemRegistrarQuintero;
    private javax.swing.JMenuItem jMenuListarContratos;
    private javax.swing.JMenuItem jMenuMedidor;
    private javax.swing.JMenuItem jMenuNuevoContrato;
    private javax.swing.JMenuItem jMenuPuesto;
    private javax.swing.JMenuItem jMenuRegMedidor;
    private javax.swing.JMenuItem jMenuRegistrarPuesto;
    private javax.swing.JMenuItem jMenuRegistrarSector;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JMenuItem menuConsultarCliente;
    private javax.swing.JMenuItem menuConsultarMedidor;
    private javax.swing.JMenuItem menuConsultarSector;
    private javax.swing.JMenuItem menuConsutlarPuesto;
    // End of variables declaration//GEN-END:variables
}